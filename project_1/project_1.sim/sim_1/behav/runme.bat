@echo off


rem  PlanAhead(TM)
rem  runme.bat: a PlanAhead-generated ISim simulation Script
rem  Copyright 1986-1999, 2001-2013 Xilinx, Inc. All Rights Reserved.


set PATH=%XILINX%\lib\%PLATFORM%;%XILINX%\bin\%PLATFORM%;C:/Xilinx/14.7/ISE_DS/EDK/bin/nt;C:/Xilinx/14.7/ISE_DS/EDK/lib/nt;C:/Xilinx/14.7/ISE_DS/ISE/bin/nt;C:/Xilinx/14.7/ISE_DS/ISE/lib/nt;C:/Xilinx/14.7/ISE_DS/common/bin/nt;C:/Xilinx/14.7/ISE_DS/common/lib/nt;C:/Xilinx/14.7/ISE_DS/PlanAhead/bin;%PATH%

set XILINX_PLANAHEAD=C:/Xilinx/14.7/ISE_DS/PlanAhead

fuse -intstyle pa -incremental --nodebug -L work -L unisims_ver -L unimacro_ver -L xilinxcorelib_ver -L secureip -o Traffic_Light_Controller_TB.exe --prj //Mac/Home/Desktop/VLSI/Miniproject/project_1/project_1.sim/sim_1/behav/Traffic_Light_Controller_TB.prj -top work.Traffic_Light_Controller_TB -top work.glbl
if errorlevel 1 (
   cmd /c exit /b %errorlevel%
)
