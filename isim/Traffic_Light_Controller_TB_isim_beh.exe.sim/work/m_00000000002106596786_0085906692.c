/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "//Mac/Home/Desktop/VLSI/Miniproject/traffic.v";
static int ng1[] = {1, 0};
static int ng2[] = {0, 0};
static unsigned int ng3[] = {1U, 0U};
static unsigned int ng4[] = {4U, 0U};
static unsigned int ng5[] = {2U, 0U};
static unsigned int ng6[] = {0U, 0U};



static void Always_27_0(char *t0)
{
    char t6[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;
    int t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;

LAB0:    t1 = (t0 + 4688U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(27, ng0);
    t2 = (t0 + 5256);
    *((int *)t2) = 1;
    t3 = (t0 + 4720);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(28, ng0);

LAB5:    xsi_set_current_line(29, ng0);
    t4 = (t0 + 2568U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(39, ng0);
    t2 = (t0 + 3768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);

LAB14:    t5 = (t0 + 472);
    t7 = *((char **)t5);
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t7, 32);
    if (t30 == 1)
        goto LAB15;

LAB16:    t2 = (t0 + 608);
    t3 = *((char **)t2);
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t3, 32);
    if (t30 == 1)
        goto LAB17;

LAB18:    t2 = (t0 + 744);
    t3 = *((char **)t2);
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t3, 32);
    if (t30 == 1)
        goto LAB19;

LAB20:    t2 = (t0 + 880);
    t3 = *((char **)t2);
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t3, 32);
    if (t30 == 1)
        goto LAB21;

LAB22:    t2 = (t0 + 1016);
    t3 = *((char **)t2);
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t3, 32);
    if (t30 == 1)
        goto LAB23;

LAB24:    t2 = (t0 + 1152);
    t3 = *((char **)t2);
    t30 = xsi_vlog_unsigned_case_compare(t4, 3, t3, 32);
    if (t30 == 1)
        goto LAB25;

LAB26:
LAB28:
LAB27:    xsi_set_current_line(106, ng0);
    t2 = (t0 + 472);
    t3 = *((char **)t2);
    t2 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);

LAB29:
LAB12:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(30, ng0);

LAB13:    xsi_set_current_line(31, ng0);
    t28 = (t0 + 472);
    t29 = *((char **)t28);
    t28 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t28, t29, 0, 0, 3, 0LL);
    xsi_set_current_line(32, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 5, 0LL);
    goto LAB12;

LAB15:    xsi_set_current_line(40, ng0);
    t5 = (t0 + 3608);
    t8 = (t5 + 56U);
    t21 = *((char **)t8);
    t22 = (t0 + 1288);
    t28 = *((char **)t22);
    memset(t6, 0, 8);
    t22 = (t21 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB31;

LAB30:    t29 = (t28 + 4);
    if (*((unsigned int *)t29) != 0)
        goto LAB31;

LAB34:    if (*((unsigned int *)t21) < *((unsigned int *)t28))
        goto LAB32;

LAB33:    t32 = (t6 + 4);
    t9 = *((unsigned int *)t32);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB35;

LAB36:    xsi_set_current_line(46, ng0);

LAB39:    xsi_set_current_line(47, ng0);
    t2 = (t0 + 608);
    t3 = *((char **)t2);
    t2 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    xsi_set_current_line(48, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 5, 0LL);

LAB37:    goto LAB29;

LAB17:    xsi_set_current_line(50, ng0);
    t2 = (t0 + 3608);
    t5 = (t2 + 56U);
    t7 = *((char **)t5);
    t8 = (t0 + 1560);
    t21 = *((char **)t8);
    memset(t6, 0, 8);
    t8 = (t7 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB41;

LAB40:    t22 = (t21 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB41;

LAB44:    if (*((unsigned int *)t7) < *((unsigned int *)t21))
        goto LAB42;

LAB43:    t29 = (t6 + 4);
    t9 = *((unsigned int *)t29);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB45;

LAB46:    xsi_set_current_line(57, ng0);

LAB49:    xsi_set_current_line(58, ng0);
    t2 = (t0 + 744);
    t3 = *((char **)t2);
    t2 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    xsi_set_current_line(59, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 5, 0LL);

LAB47:    goto LAB29;

LAB19:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 3608);
    t5 = (t2 + 56U);
    t7 = *((char **)t5);
    t8 = (t0 + 1424);
    t21 = *((char **)t8);
    memset(t6, 0, 8);
    t8 = (t7 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB51;

LAB50:    t22 = (t21 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB51;

LAB54:    if (*((unsigned int *)t7) < *((unsigned int *)t21))
        goto LAB52;

LAB53:    t29 = (t6 + 4);
    t9 = *((unsigned int *)t29);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB55;

LAB56:    xsi_set_current_line(68, ng0);

LAB59:    xsi_set_current_line(69, ng0);
    t2 = (t0 + 880);
    t3 = *((char **)t2);
    t2 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    xsi_set_current_line(70, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 5, 0LL);

LAB57:    goto LAB29;

LAB21:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 3608);
    t5 = (t2 + 56U);
    t7 = *((char **)t5);
    t8 = (t0 + 1560);
    t21 = *((char **)t8);
    memset(t6, 0, 8);
    t8 = (t7 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB61;

LAB60:    t22 = (t21 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB61;

LAB64:    if (*((unsigned int *)t7) < *((unsigned int *)t21))
        goto LAB62;

LAB63:    t29 = (t6 + 4);
    t9 = *((unsigned int *)t29);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB65;

LAB66:    xsi_set_current_line(79, ng0);

LAB69:    xsi_set_current_line(80, ng0);
    t2 = (t0 + 1016);
    t3 = *((char **)t2);
    t2 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    xsi_set_current_line(81, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 5, 0LL);

LAB67:    goto LAB29;

LAB23:    xsi_set_current_line(83, ng0);
    t2 = (t0 + 3608);
    t5 = (t2 + 56U);
    t7 = *((char **)t5);
    t8 = (t0 + 1696);
    t21 = *((char **)t8);
    memset(t6, 0, 8);
    t8 = (t7 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB71;

LAB70:    t22 = (t21 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB71;

LAB74:    if (*((unsigned int *)t7) < *((unsigned int *)t21))
        goto LAB72;

LAB73:    t29 = (t6 + 4);
    t9 = *((unsigned int *)t29);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB75;

LAB76:    xsi_set_current_line(90, ng0);

LAB79:    xsi_set_current_line(91, ng0);
    t2 = (t0 + 1152);
    t3 = *((char **)t2);
    t2 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    xsi_set_current_line(92, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 5, 0LL);

LAB77:    goto LAB29;

LAB25:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 3608);
    t5 = (t2 + 56U);
    t7 = *((char **)t5);
    t8 = (t0 + 1560);
    t21 = *((char **)t8);
    memset(t6, 0, 8);
    t8 = (t7 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB81;

LAB80:    t22 = (t21 + 4);
    if (*((unsigned int *)t22) != 0)
        goto LAB81;

LAB84:    if (*((unsigned int *)t7) < *((unsigned int *)t21))
        goto LAB82;

LAB83:    t29 = (t6 + 4);
    t9 = *((unsigned int *)t29);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB85;

LAB86:    xsi_set_current_line(102, ng0);

LAB89:    xsi_set_current_line(103, ng0);
    t2 = (t0 + 472);
    t3 = *((char **)t2);
    t2 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    xsi_set_current_line(104, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 5, 0LL);

LAB87:    goto LAB29;

LAB31:    t31 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB33;

LAB32:    *((unsigned int *)t6) = 1;
    goto LAB33;

LAB35:    xsi_set_current_line(41, ng0);

LAB38:    xsi_set_current_line(42, ng0);
    t33 = (t0 + 472);
    t34 = *((char **)t33);
    t33 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t33, t34, 0, 0, 3, 0LL);
    xsi_set_current_line(43, ng0);
    t2 = (t0 + 3608);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng1)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 5, t7, 32);
    t8 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t8, t6, 0, 0, 5, 0LL);
    goto LAB37;

LAB41:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB43;

LAB42:    *((unsigned int *)t6) = 1;
    goto LAB43;

LAB45:    xsi_set_current_line(51, ng0);

LAB48:    xsi_set_current_line(52, ng0);
    t31 = (t0 + 608);
    t32 = *((char **)t31);
    t31 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t31, t32, 0, 0, 3, 0LL);
    xsi_set_current_line(53, ng0);
    t2 = (t0 + 3608);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng1)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 5, t7, 32);
    t8 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t8, t6, 0, 0, 5, 0LL);
    goto LAB47;

LAB51:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB53;

LAB52:    *((unsigned int *)t6) = 1;
    goto LAB53;

LAB55:    xsi_set_current_line(62, ng0);

LAB58:    xsi_set_current_line(63, ng0);
    t31 = (t0 + 744);
    t32 = *((char **)t31);
    t31 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t31, t32, 0, 0, 3, 0LL);
    xsi_set_current_line(64, ng0);
    t2 = (t0 + 3608);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng1)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 5, t7, 32);
    t8 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t8, t6, 0, 0, 5, 0LL);
    goto LAB57;

LAB61:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB63;

LAB62:    *((unsigned int *)t6) = 1;
    goto LAB63;

LAB65:    xsi_set_current_line(73, ng0);

LAB68:    xsi_set_current_line(74, ng0);
    t31 = (t0 + 880);
    t32 = *((char **)t31);
    t31 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t31, t32, 0, 0, 3, 0LL);
    xsi_set_current_line(75, ng0);
    t2 = (t0 + 3608);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng1)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 5, t7, 32);
    t8 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t8, t6, 0, 0, 5, 0LL);
    goto LAB67;

LAB71:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB73;

LAB72:    *((unsigned int *)t6) = 1;
    goto LAB73;

LAB75:    xsi_set_current_line(84, ng0);

LAB78:    xsi_set_current_line(85, ng0);
    t31 = (t0 + 1016);
    t32 = *((char **)t31);
    t31 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t31, t32, 0, 0, 3, 0LL);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 3608);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng1)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 5, t7, 32);
    t8 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t8, t6, 0, 0, 5, 0LL);
    goto LAB77;

LAB81:    t28 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB83;

LAB82:    *((unsigned int *)t6) = 1;
    goto LAB83;

LAB85:    xsi_set_current_line(96, ng0);

LAB88:    xsi_set_current_line(97, ng0);
    t31 = (t0 + 1152);
    t32 = *((char **)t31);
    t31 = (t0 + 3768);
    xsi_vlogvar_wait_assign_value(t31, t32, 0, 0, 3, 0LL);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 3608);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t7 = ((char*)((ng1)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t5, 5, t7, 32);
    t8 = (t0 + 3608);
    xsi_vlogvar_wait_assign_value(t8, t6, 0, 0, 5, 0LL);
    goto LAB87;

}

static void Always_110_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    int t9;
    char *t10;

LAB0:    t1 = (t0 + 4936U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(110, ng0);
    t2 = (t0 + 5272);
    *((int *)t2) = 1;
    t3 = (t0 + 4968);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(111, ng0);

LAB5:    xsi_set_current_line(113, ng0);
    t4 = (t0 + 3768);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);

LAB6:    t7 = (t0 + 472);
    t8 = *((char **)t7);
    t9 = xsi_vlog_unsigned_case_compare(t6, 3, t8, 32);
    if (t9 == 1)
        goto LAB7;

LAB8:    t2 = (t0 + 608);
    t3 = *((char **)t2);
    t9 = xsi_vlog_unsigned_case_compare(t6, 3, t3, 32);
    if (t9 == 1)
        goto LAB9;

LAB10:    t2 = (t0 + 744);
    t3 = *((char **)t2);
    t9 = xsi_vlog_unsigned_case_compare(t6, 3, t3, 32);
    if (t9 == 1)
        goto LAB11;

LAB12:    t2 = (t0 + 880);
    t3 = *((char **)t2);
    t9 = xsi_vlog_unsigned_case_compare(t6, 3, t3, 32);
    if (t9 == 1)
        goto LAB13;

LAB14:    t2 = (t0 + 1016);
    t3 = *((char **)t2);
    t9 = xsi_vlog_unsigned_case_compare(t6, 3, t3, 32);
    if (t9 == 1)
        goto LAB15;

LAB16:    t2 = (t0 + 1152);
    t3 = *((char **)t2);
    t9 = xsi_vlog_unsigned_case_compare(t6, 3, t3, 32);
    if (t9 == 1)
        goto LAB17;

LAB18:
LAB20:
LAB19:    xsi_set_current_line(158, ng0);

LAB28:    xsi_set_current_line(159, ng0);
    t2 = ((char*)((ng6)));
    t3 = (t0 + 2968);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(160, ng0);
    t2 = ((char*)((ng6)));
    t3 = (t0 + 3448);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(161, ng0);
    t2 = ((char*)((ng6)));
    t3 = (t0 + 3288);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(162, ng0);
    t2 = ((char*)((ng6)));
    t3 = (t0 + 3128);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);

LAB21:    goto LAB2;

LAB7:    xsi_set_current_line(116, ng0);

LAB22:    xsi_set_current_line(117, ng0);
    t7 = ((char*)((ng3)));
    t10 = (t0 + 2968);
    xsi_vlogvar_wait_assign_value(t10, t7, 0, 0, 3, 0LL);
    xsi_set_current_line(118, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 3448);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(119, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3288);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(120, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3128);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB21;

LAB9:    xsi_set_current_line(123, ng0);

LAB23:    xsi_set_current_line(124, ng0);
    t2 = ((char*)((ng3)));
    t4 = (t0 + 2968);
    xsi_vlogvar_wait_assign_value(t4, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(125, ng0);
    t2 = ((char*)((ng5)));
    t3 = (t0 + 3448);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(126, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3288);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(127, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3128);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB21;

LAB11:    xsi_set_current_line(130, ng0);

LAB24:    xsi_set_current_line(131, ng0);
    t2 = ((char*)((ng3)));
    t4 = (t0 + 2968);
    xsi_vlogvar_wait_assign_value(t4, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(132, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3448);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(133, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 3288);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(134, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3128);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB21;

LAB13:    xsi_set_current_line(137, ng0);

LAB25:    xsi_set_current_line(138, ng0);
    t2 = ((char*)((ng5)));
    t4 = (t0 + 2968);
    xsi_vlogvar_wait_assign_value(t4, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(139, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3448);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(140, ng0);
    t2 = ((char*)((ng5)));
    t3 = (t0 + 3288);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(141, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3128);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB21;

LAB15:    xsi_set_current_line(144, ng0);

LAB26:    xsi_set_current_line(145, ng0);
    t2 = ((char*)((ng4)));
    t4 = (t0 + 2968);
    xsi_vlogvar_wait_assign_value(t4, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(146, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3448);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(147, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3288);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(148, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 3128);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB21;

LAB17:    xsi_set_current_line(151, ng0);

LAB27:    xsi_set_current_line(152, ng0);
    t2 = ((char*)((ng4)));
    t4 = (t0 + 2968);
    xsi_vlogvar_wait_assign_value(t4, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(153, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3448);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(154, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3288);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    xsi_set_current_line(155, ng0);
    t2 = ((char*)((ng5)));
    t3 = (t0 + 3128);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 3, 0LL);
    goto LAB21;

}


extern void work_m_00000000002106596786_0085906692_init()
{
	static char *pe[] = {(void *)Always_27_0,(void *)Always_110_1};
	xsi_register_didat("work_m_00000000002106596786_0085906692", "isim/Traffic_Light_Controller_TB_isim_beh.exe.sim/work/m_00000000002106596786_0085906692.didat");
	xsi_register_executes(pe);
}
