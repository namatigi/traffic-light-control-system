`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: Leonard Joseph, Stanley Mollel, Masoud Abdallah,Emmanuel Kahigwa
// Module Name: Traffic_Light_Controller
// Project Name: Mini Project
// Target Devices: FPGA
//////////////////////////////////////////////////////////////////////////////////


module Traffic_Light_Controller(


    input clk,rst,
    output reg [2:0]light_M1,
    output reg [2:0]light_S,
    output reg [2:0]light_MT,
    output reg [2:0]light_M2
    );
    
    parameter  State_1=0, State_2=1, State_3 =2, State_4=3, State_5=4,State_6=5;
    reg [4:0]traffic_count;
    reg[2:0] present_state;
    parameter  timer7=7,timer5=5,timer2=2,timer3=3;

   
    
    always@(posedge clk or posedge rst) //Checking change of clock or reset.
        begin
        if(rst==1)
        begin
        present_state<=State_1;
        traffic_count<=0;
        end
        else
        
      
       
            
            case(present_state)
                State_1: if(traffic_count<timer7) 
                        begin
                        present_state<=State_1; //Assigning present state to state 1
                        traffic_count<=traffic_count+1; //Incrementing the counter
                        end
                    else
                        begin
                        present_state<=State_2; //Transitioning to the next state
                        traffic_count<=0; // Resetting traffic counter
                        end
                State_2: if(traffic_count<timer2)
                        begin
                        present_state<=State_2; //Assigning present state to state 2
                        traffic_count<=traffic_count+1; //Incrementing the counter
                        end

                    else
                        begin
                        present_state<=State_3;//Transitioning to the next state
                        traffic_count<=0;// Resetting traffic counter
                        end
                State_3: if(traffic_count<timer5)
                        begin
                        present_state<=State_3; //Assigning present state to state 3
                        traffic_count<=traffic_count+1;  //Incrementing the counter
                        end

                    else
                        begin
                        present_state<=State_4;//Transitioning to the next state
                        traffic_count<=0;// Resetting traffic counter
                        end
                State_4:if(traffic_count<timer2)
                        begin
                        present_state<=State_4; //Assigning present state to state 4
                        traffic_count<=traffic_count+1;//Incrementing the counter
                        end

                    else
                        begin
                        present_state<=State_5;//Transitioning to the next state
                        traffic_count<=0; // Resetting traffic counter
                        end
                State_5:if(traffic_count<timer3)
                        begin
                        present_state<=State_5; //Assigning present state to state 5
                        traffic_count<=traffic_count+1; //Incrementing the counter
                        end

                    else
                        begin
                        present_state<=State_6; //Transitioning to the next state
                        traffic_count<=0; // Resetting traffic counter
                        end

                State_6:if(traffic_count<timer2)
                        begin
                        present_state<=State_6; //Assigning present state to state 5
                        traffic_count<=traffic_count+1;//Incrementing the counter
                        end

                    else
                        begin
                        present_state<=State_1; //Looping back to state 1
                        traffic_count<=0; // Resetting traffic counter
                        end
                default: present_state<=State_1; //In case of anything assign state 1 as default state
                endcase
            end   

            always@(present_state)   //Checking any change of state  
            begin
                
                case(present_state)
                     
                    State_1:
                    begin
                       light_M1<=3'b001;
                       light_M2<=3'b001;
                       light_MT<=3'b100;
                       light_S<=3'b100;
                    end
                    State_2:
                    begin 
                       light_M1<=3'b001;
                       light_M2<=3'b010;
                       light_MT<=3'b100;
                       light_S<=3'b100;
                    end
                    State_3:
                    begin
                       light_M1<=3'b001;
                       light_M2<=3'b100;
                       light_MT<=3'b001;
                       light_S<=3'b100;
                    end
                    State_4:
                    begin
                       light_M1<=3'b010;
                       light_M2<=3'b100;
                       light_MT<=3'b010;
                       light_S<=3'b100;
                    end
                    State_5:
                    begin
                       light_M1<=3'b100;
                       light_M2<=3'b100;
                       light_MT<=3'b100;
                       light_S<=3'b001;
                    end
                    State_6:
                    begin 
                       light_M1<=3'b100;
                       light_M2<=3'b100;
                       light_MT<=3'b100;
                       light_S<=3'b010;
                    end
                    default:
                    begin 
                       light_M1<=3'b000;
                       light_M2<=3'b000;
                       light_MT<=3'b000;
                       light_S<=3'b000;
                    end
                    endcase
            end                
              

endmodule
